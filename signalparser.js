//server listener for clients
var http = require('http');
var io = require('socket.io')(4000);

//redis-http coms
var cookie_reader = require('cookie');
var url  = require('url');
var querystring = require('querystring');

//server listener for arduino
var serialport = require('serialport');
var SerialPort = serialport.SerialPort;

var portName = '/dev/ttyACM0';
var array = [];
var itt = 0;
var msg = "";

var sp = new serialport.SerialPort(portName, {
    baudRate: 9600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false,
    parser: serialport.parsers.readline("\r\n")
});

io.sockets.on('connection', function (socket) {
 sp.on('data', function(input) {
    array[itt] = input;
    itt+=1;
    if(array.length >= 1200){
      senddata();
    }
    socket.send(array[itt-1]);
  });

  function senddata(){
    console.log("send data direct");

    values = querystring.stringify({
        title: "gsr",
        data: array.toString(),
    });
    console.log("The array is of size: " + array.length);
    array = [];
    itt = 0;
    var options = {
        host: 'localhost',
        path: '/control/gsrsample/',
        port: 8000,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': values.length
        }
    };
    try{
      //Send message to Django server
      var req = http.request(options, function(res){
          res.setEncoding('utf8');
          //Print out error message
          res.on('data', function(message){
            console.log(message);
          });
      });
      req.write(values);
      req.end();
    }catch(ex){
      console.log(ex);
    }
  }
  //do something with cookies
  io.use(function(socket,accept){
      var data = socket.request;
      if(data.headers.cookie){
          data.cookie = cookie_reader.parse(data.headers.cookie);
          return accept(null, true);
      }
      return accept('error', false);
  });






  /*
  socket.on('send_message', function (message) {
    console.log("we're here");

    values = querystring.stringify({
        title: "gsr",
        data: array.toString(),
    });
    console.log("The array is of size: " + array.length);
    array = [];
    itt = 0;
    var options = {
        host: 'localhost',
        path: '/control/gsrsample/',
        port: 8000,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': values.length
        }
    };
    try{
      //Send message to Django server
      var req = http.request(options, function(res){
          res.setEncoding('utf8');
          //Print out error message
          res.on('data', function(message){
            console.log(message);
          });
      });
      req.write(values);
      req.end();
    }catch(ex){
      console.log(ex);
    }
}); */
});
